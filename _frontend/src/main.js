import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// Using import statements...
// import 'jquery'
// import 'bootstrap'
// ...work until here, where 'yarn serve' tells my that patternfly is NOT installed
// despite the fact it shows up in npm_modules
// import 'patternfly'
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
  computed: {
    databases () {
      return store.state.dbs
    }
  }
}).$mount('#app')
