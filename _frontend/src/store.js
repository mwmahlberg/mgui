import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    dbs: [],
    dbDetails: []
  },
  mutations: {
    updateDBs (state, payload) {
      payload.then(value => {
        console.log(value)
        state.dbs = value
      })
      // state.dbs = payload
    },
    updateDbDetails (state, payload) {
      payload.then(value => {
        console.log(value)
        state.dbDetails[value.db] = value
      })
    }
  },
  actions: {
    refreshDatabases ({ commit }) {
      console.log('Refresh called')
      fetch('http://localhost:27020/databases').then((response) => {
        if (response.status === 200) {
          // We commit a promise to the store
          // Only when we actually want to set the value, it is evaluated
          commit('updateDBs', response.json())
        } else {
          alert('Error: ' + response.statusText)
        }
      }).catch(error => console.log(error))
    },
    getDatabaseDetails ({ commit }, db) {
      console.log('Fetching database details')
      fetch('http://localhost:27020/database/' + db).then((response) => {
        if (response.status === 200) {
          commit('updateDbDetails', response.json())
        }
      })
    }
  }
})
