package api

import (
	"context"
	"net/http"
)

type AuthField int

const (
	UserField AuthField = iota
	PasswordField
)

func AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		if user, pass, ok := r.BasicAuth(); ok {
			ctx := context.WithValue(context.Background(), UserField, user)
			ctx = context.WithValue(ctx, PasswordField, pass)
			next.ServeHTTP(w, r.WithContext(ctx))
			return
		}

		http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
	})
}
