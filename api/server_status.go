package api

// {
// 	"host" : "1edb3d346cfa",
// 	"version" : "3.4.18",
// 	"process" : "mongod",
// 	"pid" : NumberLong(1),
// 	"uptime" : 162405,
// 	"uptimeMillis" : NumberLong(162405090),
// 	"uptimeEstimate" : NumberLong(162405),
// 	"localTime" : ISODate("2019-01-01T15:11:57.560Z"),
// 	"asserts" : {
// 		"regular" : 0,
// 		"warning" : 0,
// 		"msg" : 0,
// 		"user" : 49376,
// 		"rollovers" : 0
// 	},
// 	"connections" : {
// 		"current" : 6,
// 		"available" : 838854,
// 		"totalCreated" : 24689
// 	},

type ConnectionStats struct {
	Current      int
	Available    int
	TotalCreated int `bson:"totalCreated"`
}

type ServerState struct {
	Host         string
	Version      string
	Process      string
	PID          int
	Uptime       int64
	UptimeMillis int64
	Connections  ConnectionStats
}
