package api

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

var (
	serverStatusCmd = bson.M{"serverStatus": 1}
)

func ServerStatus(parent *mgo.Session) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		sess := parent.Copy()
		defer sess.Close()

		// user, _ := r.Context().Value(UserField).(string)
		// pass, _ := r.Context().Value(UserField).(string)

		// sess.LogoutAll()
		// if err := sess.Login(&mgo.Credential{
		// 	Username: user,
		// 	Password: pass,
		// 	Source:   "admin",
		// }); err != nil {
		// 	http.Error(w, err.Error(), http.StatusBadRequest)
		// 	return
		// }

		status := &bson.M{}
		if err := sess.Run(serverStatusCmd, status); err != nil {
			if e, ok := err.(*mgo.QueryError); ok {
				log.Printf("Error querying : %s", e.Message)
				http.Error(w, http.StatusText(http.StatusBadRequest)+": "+e.Message, http.StatusBadRequest)
			}
		}

		if err := json.NewEncoder(w).Encode(status); err != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError)+": "+err.Error(), http.StatusInternalServerError)
		}
	})
}
