// Copyright © 2019 Markus Mahlberg <markus@mahlberg.io>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"log"
	"net"
	"net/http"

	"bitbucket.org/mwmahlberg/mgui/api"
	"bitbucket.org/mwmahlberg/mgui/web"
	"github.com/globalsign/mgo"

	"github.com/spf13/viper"

	rice "github.com/GeertJohan/go.rice"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/spf13/cobra"
)

// serverCmd represents the server command
var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "Web GUI server for MongoDB",
	Run:   runServer,
}

func init() {
	rootCmd.AddCommand(serverCmd)

	serverCmd.Flags().Uint16P("port", "p", 27020, "port to bind to")
	serverCmd.Flags().IP("ip", net.ParseIP("0.0.0.0"), "ip address to bind to")
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// serverCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// serverCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	viper.BindPFlag("ip", serverCmd.LocalFlags().Lookup("ip"))
	viper.BindPFlag("port", serverCmd.LocalFlags().Lookup("port"))
}

func runServer(cmd *cobra.Command, args []string) {

	var (
		ip   = viper.GetString("ip")
		port = viper.GetInt("port")
	)

	r := mux.NewRouter()

	r.Handle("/ping", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Server", "mgui")
		w.WriteHeader(http.StatusNoContent)
	}))

	sess, _ := mgo.DialWithInfo(&mgo.DialInfo{Username: "root", Password: "example", Addrs: []string{"127.0.0.1:27017"}})
	defer sess.Close()
	r.Path("/database/{db:[a-zA-Z]+}").Methods(http.MethodGet).Handler(web.GetDatabaseDetails(sess))
	r.Path("/databases").Methods(http.MethodGet).Handler(web.GetDatabases(sess))
	r.Handle("/serverstatus", api.ServerStatus(sess))

	box := rice.MustFindBox("../_frontend/dist/")

	r.PathPrefix("/").Handler(http.FileServer(box.HTTPBox()))

	cors := handlers.CORS(handlers.AllowedOrigins([]string{"*"}))
	log.Printf("Starting mgui %s listening to %s:%d", version, ip, port)
	http.ListenAndServe(fmt.Sprintf("%s:%d", ip, port), cors(r))
}
