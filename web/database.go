package web

import (
	"encoding/json"
	"net/http"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/gorilla/mux"
)

func GetDatabaseDetails(session *mgo.Session) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		c := session.Copy()
		defer c.Close()

		vars := mux.Vars(r)

		dbname := vars["db"]
		result := bson.M{}
		if err := c.DB(dbname).Run("dbstats", &result); err != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError)+": "+err.Error(), http.StatusInternalServerError)
			return
		}

		enc := json.NewEncoder(w)
		if err := enc.Encode(&result); err != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError)+": "+err.Error(), http.StatusInternalServerError)
		}
	})
}
