package web

import (
	"encoding/json"
	"net/http"

	"github.com/globalsign/mgo"
)

func GetDatabases(session *mgo.Session) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		s := session.Copy()
		var names []string
		var err error
		if names, err = s.DatabaseNames(); err != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
		enc := json.NewEncoder(w)

		if err = enc.Encode(&names); err != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		}
	})
}
